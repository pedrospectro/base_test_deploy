var frisby = require('frisby');
var api_config = require('./config.api.js');
var api_data = require('./config.data.js');

var routes = api_config.routes;

frisby.create('Testing user route without token')
	.get(routes.user)
	.inspectBody()
	.expectStatus(api_config.status.bad_request)
.toss()

frisby.create('Testing user index')
	.addHeader('Authorization', api_data.user_login.api_key)
	.get(routes.user)
	.inspectBody()
	.expectStatus(api_config.status.not_found)
.toss()

frisby.create('Testing user get')
	.addHeader('Authorization', api_data.user_login.api_key)
	.get(routes.user+api_data.user_login.id)
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()

frisby.create('Testing user geting another user')
	.addHeader('Authorization', api_data.user_login.api_key)
	.get(routes.user+api_data.admin_login.id)
	.inspectBody()
	.expectStatus(api_config.status.bad_request)
.toss()

frisby.create('Testing user put')
	.addHeader('Authorization', api_data.user_login.api_key)
	.put(routes.user+api_data.user_login.id,{name:"teste"},{json:true})
	.inspectBody()
	.expectStatus(api_config.status.already_reported)
.toss()

frisby.create('Testing user create')
	.addHeader('Authorization', api_data.user_login.api_key)
	.post(routes.user,api_data.user_signup,{json:true})
	.inspectBody()
	.expectStatus(api_config.status.not_found)
.toss()

frisby.create('Testing user delete')
	.addHeader('Authorization', api_data.user_login.api_key)
	.delete(routes.user+api_data.user_login.id)
	.inspectBody()
	.expectStatus(api_config.status.already_reported)
.toss()