module.exports = {    
    
    address_id:{id:"1"},

    admin_login:{
    	id    : "1",
        email : 'test_admin@etools.com.br',
        password : 'x6d4h3c9',
        api_key : 'Token token=test_admin_token'
    },

    user_login:{
    	id    : "2",
        email : 'test_user@etools.com.br',
        password : 'teste123',
        api_key : 'Token token=test_user_token'
    },

	user_signup:{
        email : 'signup@test.com.br',
        password : '123test123',
        name : 'test signup'
	},

    admin_new_user:{
        email : 'admin_new_user@test.com.br',
        password : '123test123',
        name : 'test signup'
    },

    user_inserted:{
        email : 'inserted@test.com.br',
        password : '123test123',
        name : 'test signup'
    },

	address_data:{
	    cep: '68743010',
	    city: 'Castanhal',
	    country: 'BR',
	    uf: 'PA',
	   	street: 'Quintino bocaiuva',
	    neigborhood: 'CENTRO'
	},

	nome_obscuro: function(n){
		var nome = "";
    	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-";
    	for( var i=0; i < n; i++ )
        	nome += possible.charAt(Math.floor(Math.random() * possible.length));
        return nome;
	},

	hora_inicio_obscuro: function(){
		var nome = "0";
    	var possible = "123456789";
        nome += possible.charAt(Math.floor(Math.random() * possible.length));
        return nome;
	},

	hora_fim_obscuro: function(){
		var nome = "1";
    	var possible = "123456789";
        nome += possible.charAt(Math.floor(Math.random() * possible.length));
        return nome;
	},

	ano_obscuro: function(){
		var nome = "";
    	var possible = "0123456789";
    	for(var i=0; i < 4; i++ )
        	nome += possible.charAt(Math.floor(Math.random() * possible.length));
        return nome;
	},

	mes_obscuro: function(){
		var nome = "";
    	var possible_first = "01";
    	var possible_second = "012";
    	var possible = "0123456789";
    	nome += possible_first.charAt(Math.floor(Math.random() * possible_first.length));
    	if(nome=="1")
    		nome += possible_second.charAt(Math.floor(Math.random() * possible_second.length));
        else	
        	nome += possible.charAt(Math.floor(Math.random() * possible.length));

        return nome;
	},

	dia_obscuro: function(){
		var nome = "";
    	var possible_first = "0123";
    	var possible_second = "01";
    	var possible = "0123456789";
    	nome += possible_first.charAt(Math.floor(Math.random() * possible_first.length));
    	if(nome=="3")
    		nome += possible_second.charAt(Math.floor(Math.random() * possible_second.length));
        else	
        	nome += possible.charAt(Math.floor(Math.random() * possible.length));
        return nome;
	},

	cpf_obscuro: function(){
		var n = 9;
		var n1 = Math.round(Math.random()*n);
		var n2 = Math.round(Math.random()*n);
		var n3 = Math.round(Math.random()*n);
		var n4 = Math.round(Math.random()*n);
		var n5 = Math.round(Math.random()*n);
		var n6 = Math.round(Math.random()*n);
		var n7 = Math.round(Math.random()*n);
		var n8 = Math.round(Math.random()*n);
		var n9 = Math.round(Math.random()*n);
		var d1 = n9*2+n8*3+n7*4+n6*5+n5*6+n4*7+n3*8+n2*9+n1*10;
		d1 = 11 - (Math.round(d1 - (Math.floor(d1/11)*11)));
		if (d1>=10) d1 = 0;
		var d2 = d1*2+n9*3+n8*4+n7*5+n6*6+n5*7+n4*8+n3*9+n2*10+n1*11;
		d2 = 11 - (Math.round(d2 - (Math.floor(d2/11)*11)));
		if (d2>=10) d2 = 0;
		return ''+n1+n2+n3+n4+n5+n6+n7+n8+n9+d1+d2;
	},

	email_obscuro: function(){
		var user = "";var host="";
    	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    	for( var i=0; i < 5; i++ ){
        	user += possible.charAt(Math.floor(Math.random() * possible.length));
        	host += possible.charAt(Math.floor(Math.random() * possible.length));
    	}
    	return user+'@'+host;
	},

	cep_obscuro: function(){
		var cep = "";
    	var possible = "0123456789";
    	for( var i=0; i < 8; i++ ){
        	cep += possible.charAt(Math.floor(Math.random() * possible.length));
    	}
    	return cep;
	},

	fix_param_route: function(id,str,previous){
		return str.replace(previous,id.toString());
	}
};