var frisby = require('frisby');
var api_config = require('./config.api.js');
var api_data = require('./config.data.js');

var routes = api_config.routes;

frisby.create('Testing role route without token')
	.get(routes.admin_roles_users)
	.inspectBody()
	.expectStatus(api_config.status.bad_request)
.toss()

frisby.create('Testing role index')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.get(api_data.fix_param_route(3,routes.admin_roles_users,":user_id"))
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()

frisby.create('Testing role get')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.get(api_data.fix_param_route(3,routes.admin_roles_users,":user_id")+"2")
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()

frisby.create('Testing role put')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.put(api_data.fix_param_route(3,routes.admin_roles_users,":user_id")+"1",{name:"test"},{json:true})
	.inspectBody()
	.expectStatus(api_config.status.not_found)
.toss()

frisby.create('Testing role create')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.post(api_data.fix_param_route(3,routes.admin_roles_users,":user_id"),{role_id:1},{json:true})
	.inspectBody()
	.expectStatus(api_config.status.created)
.toss()

frisby.create('Testing  delete')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.delete(api_data.fix_param_route(3,routes.admin_roles_users,":user_id")+"1")
	.inspectBody()
	.expectStatus(api_config.status.already_reported)
.toss()