var url = require('./config.url.js');

module.exports = {

    status:{
	    ok: 200,
		created: 201,
		already_reported: 208,
		bad_request: 400,
		not_found: 404,
		unprocessable_entity: 422
	},

	routes:{
		test:url.local+'test/',
	    signup:url.local+'signup/',
	    login:url.local+'login/',
	    user:url.local+'users/',
	    address:url.local+'addresses/',
	   	admin_user:url.local+'admin/users/',
	   	admin_address:url.local+'admin/addresses/',
	   	admin_roles:url.local+'admin/roles/',
	   	admin_roles_users:url.local+'admin/users/:user_id/roles_users/'
	}
	
};