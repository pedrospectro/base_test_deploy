var frisby = require('frisby');
var api_config = require('./config.api.js');
var api_data = require('./config.data.js');

frisby.create('Testing api')
	.get(api_config.routes.test)
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()