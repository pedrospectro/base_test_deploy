var frisby = require('frisby');
var api_config = require('./config.api.js');
var api_data = require('./config.data.js');

var routes = api_config.routes;

frisby.create('Testing signup')
	.post(routes.signup,api_data.user_signup,{json:true})
	.inspectBody()
	.expectStatus(api_config.status.created)
.toss()

frisby.create('Testing signup with created user')
	.post(routes.signup,api_data.user_signup,{json:true})
	.inspectBody()
	.expectStatus(api_config.status.unprocessable_entity)
.toss()

frisby.create('Testing login')
	.post(routes.login,api_data.user_signup,{json:true})
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()