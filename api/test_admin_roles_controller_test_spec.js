var frisby = require('frisby');
var api_config = require('./config.api.js');
var api_data = require('./config.data.js');

var routes = api_config.routes;

frisby.create('Testing role route without token')
	.get(routes.admin_roles)
	.inspectBody()
	.expectStatus(api_config.status.bad_request)
.toss()

frisby.create('Testing role index')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.get(routes.admin_roles)
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()

frisby.create('Testing role get')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.get(routes.admin_roles+api_data.user_login.id)
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()

frisby.create('Testing role put')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.put(routes.admin_roles+api_data.user_login.id,{name:"test"},{json:true})
	.inspectBody()
	.expectStatus(api_config.status.already_reported)
.toss()

frisby.create('Testing role create')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.post(routes.admin_roles,{name:"test role",description:"test role"},{json:true})
	.inspectBody()
	.expectStatus(api_config.status.created)
.toss()

frisby.create('Testing  delete')
	.addHeader('Authorization', api_data.admin_login.api_key)
	.delete(routes.admin_roles+api_data.user_login.id)
	.inspectBody()
	.expectStatus(api_config.status.already_reported)
.toss()