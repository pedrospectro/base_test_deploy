var frisby = require('frisby');
var api_config = require('./config.api.js');
var api_data = require('./config.data.js');

var routes = api_config.routes;

frisby.create('Testing addresses route without token')
	.get(routes.address)
	.inspectBody()
	.expectStatus(api_config.status.bad_request)
.toss()

frisby.create('Testing address index')
	.addHeader('Authorization', api_data.user_login.api_key)
	.get(routes.address)
	.inspectBody()
	.expectStatus(api_config.status.not_found)
.toss()

frisby.create('Testing address get')
	.addHeader('Authorization', api_data.user_login.api_key)
	.get(routes.address+api_data.address_id.id)
	.inspectBody()
	.expectStatus(api_config.status.ok)
.toss()

frisby.create('Testing  address put')
	.addHeader('Authorization', api_data.user_login.api_key)
	.put(routes.address+api_data.address_id.id,{city:"teste"},{json:true})
	.inspectBody()
	.expectStatus(api_config.status.not_found)
.toss()

frisby.create('Testing address create')
	.addHeader('Authorization', api_data.user_login.api_key)
	.post(routes.address,api_data.address_data,{json:true})
	.inspectBody()
	.expectStatus(api_config.status.created)
.toss()

frisby.create('Testing user delete')
	.addHeader('Authorization', api_data.user_login.api_key)
	.delete(routes.address+api_data.address_id.id)
	.inspectBody()
	.expectStatus(api_config.status.not_found)
.toss()