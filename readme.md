###########    test and deploy app    	   #######################
#                                                                #
#	Developer: Pedro Cavalcante (pedroccavalcante@gmail.com) 	 #
# 																 #
##################################################################
#		Remember: Shutdown the rails server and frontend service.#
#		   Automatic test app will make all jobs to you. 		 #
##################################################################
# 																 #
#		After install the app according the install.txt 		 #
#				you can run test and deploy						 #
#      															 #
#		This app runs a External deploy with local test.         #
# 																 #
##################################################################

						Instructions:

1)API test and deploy
	To run a API deploy you should command:

		Deploy in Stage:
			>fab stage  deploy_api:"Your Name","Your commit msg"

		Deploy in Prod:
			>fab prod  deploy_api:"Your Name","Your commit msg"

2)Front test and deploy
	To run a Frontend deploy you should command:

		Deploy in Stage:
			>fab stage  deploy_front:"Your Name","Your commit msg"

		Deploy in Prod:
			>fab prod  deploy_front:"Your Name","Your commit msg"


If all api tests and frontend tests are ok, then the app
will commit the work and deploy the application in 
environment selected.

################################################################
#                Enjoy it and Best Regards.                    #
################################################################