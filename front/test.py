# -*- coding: utf-8 -*-
import os
import sys
import time
import random
import unittest
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
#optional
#from pyvirtualdisplay import Display

class Test(unittest.TestCase):
	driver=None
	display=None
	time_to_wait = 2
	token = ''

	# Urls

	local_url = 'http://localhost:3000'
	stage_url = 'http://localhost:3000'

	# Menu Class
	menu_button_class = ''
	menu_button_xpath = ''
	menu_button_selector = ''
	
	# Use to implicity wait, this is the max time response expected to get a new element
	max_ajax_time_response_expected = 360

	#Use to wait the test database restart in test/start
	database_stablish_wait_time = 432

    #Selectors in ux
	#here goes the selectors to navigate ...


	#generate cpf
	def cpf_obscuro(self):
		n = [random.randrange(10) for a in range(9)];o=11-sum(map(int.__mul__,n,range(10+len(n)-9,1,-1)))%11;n+=[(o>=10 and[0]or[o])[0]];o=11-sum( map(int.__mul__,n,range(10+len(n)-9,1,-1)))%11;n+=[(o>=10 and[0] or[o])[0]];return"%d%d%d.%d%d%d.%d%d%d-%d%d"%tuple(n)

	@classmethod
 	def setUpClass(cls):
		cls.driver = webdriver.Chrome("./chromedriver")
		driver = cls.driver
		#Here we set the implicit wait inside the webdriver 
		cls.driver.implicitly_wait(cls.max_ajax_time_response_expected)
		driver.implicitly_wait(1)

	def kill(self):
		if self.driver is not None:
			self.driver.quit()

	def __del__(self): 
		self.kill()

	def getHome(self):
		return self.driver.get(self.stage_url)

	def getHomeWithToken(self):
		return self.driver.get(self.local_url +'?token='+ self.token)

	def getTestToken(self):
		return self.driver.get(self.local_url + self.test_setup_url)

	def getTestDispose(self):
		return self.driver.get(self.local_url + self.test_dispose_url)

	def getRoute(self,route):
		return self.driver.get(self.stage_url + '/' + route)

	def javascript(self,str):
		return self.driver.execute_script(str)

	def getId(self,id):
		time.sleep(self.time_to_wait)
		try:
			element = self.driver.find_element_by_id(id)
			if element is not None:
				return element;
		except:
			return None

	def checkIdExists(self,id):
		time.sleep(self.time_to_wait)
		try:
			element = self.driver.find_element_by_id(id)
			if element is not None:
				return True;
		except:
			return False

	def clickInPath(self,xpath):
		time.sleep(self.time_to_wait)
		element =  self.getXPath(xpath)
		element.click()

	def clickInSelector(self,selector):
		time.sleep(self.time_to_wait)
		element =  self.getSelector(selector)
		element.click()
	
	# Opens the main menu 
	def openMenu(self):
		self.clickInSelector(self.menu_button_selector)
		

	def closeMenu(self):
		time.sleep(self.time_to_wait)    		
		external_div = self.driver.find_element_by_css_selector(self.close_menu_div_selector)   
		external_div.click()
		
	def getSelector(self,selector):
		time.sleep(self.time_to_wait)    		
		try:
			element = self.driver.find_element_by_css_selector(selector)
			if element is not None:
				return element;
		except:
			return None

	def getClass(self,class_name):
		time.sleep(self.time_to_wait)
		try:
			element = self.driver.find_element_by_class(class_name)
			if element is not None:
				return element;
		except:
			return None

	def getXPath(self,xpath):
		time.sleep(self.time_to_wait)    		
		try:
			element = self.driver.find_element_by_xpath(xpath)
			if element is not None:
				return element;
		except:
			return None

	def fullFillXPath(self,xpath,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_xpath(xpath)
		ActionChains(self.driver).move_to_element(element).click().perform()
		element.clear()
		element.send_keys(data)

	def fullFillSelector(self,selector,data):
		time.sleep(self.time_to_wait)
		element = self.getSelector(selector)
		ActionChains(self.driver).move_to_element(element).click().perform();
		element.clear()
		element.send_keys(data)
		element.click()

	def fullFillSelectorAndTab(self,selector,data):
		time.sleep(self.time_to_wait)
		element = self.getSelector(selector)
		ActionChains(self.driver).move_to_element(element).click().perform();
		element.clear()
		element.send_keys(data)
		element.send_keys(Keys.TAB)
		element.click()

	def checkSelectorExists(self,selector):
		time.sleep(self.time_to_wait)
		try:
			element = self.driver.find_element_by_css_selector(selector)
			if element is not None:
				return True;
		except:
			return False

	def fullFillTagNamed(self,name,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_name(name)
		element.clear()
		element.send_keys(data)

	def fullFillClass(self,name,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_class_name(name)
		element.clear()
		element.send_keys(data)
		
	def fullFillSelectOptionNamed(self,name,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_name(name)
		for option in element.find_elements_by_tag_name('option'):
			if option.get_attribute("value") == data:
				option.click() 
				break

	def fullFillSelectOptionId(self,id,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_id(id)
		for option in element.find_elements_by_tag_name('option'):
			if option.get_attribute("value") == data:
				option.click() 
				break

	def fullFillSelectOptionXPath(self,xpath,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_xpath(xpath)
		for option in element.find_elements_by_tag_name('option'):
			if option.get_attribute("value") == data:
				option.click() 
				break

	def fullFillSelectOptionXPathDataContains(self,xpath,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_xpath(xpath)
		for option in element.find_elements_by_tag_name('option'):
			if data == option.get_attribute("value"):
				option.click() 
				break

	def fullFillSelectOptionXPathDataContainsText(self,xpath,data):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_xpath(xpath)
		for option in element.find_elements_by_tag_name('option'):
			if data == option.get_attribute("text"):
				option.click() 
				break

	def fullFillSelectOptionXPathLast(self,xpath):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_xpath(xpath)
		count = 0
		for option in element.find_elements_by_tag_name('option'):
			count=count+1
			if count == len(element.find_elements_by_tag_name('option')):
				option.click() 
				break

	def fullFillSelectOptionXPathFirst(self,xpath):
		time.sleep(self.time_to_wait)
		element = self.driver.find_element_by_xpath(xpath)
		count = 0
		for option in element.find_elements_by_tag_name('option'):
			count=count+1
			if count == 2:
				option.click() 
				break

	def fullFillSelectOptionSelectorWithArrowDown(self,selector):
		time.sleep(self.time_to_wait)
		element = self.getSelector(selector)
		ActionChains(self.driver).move_to_element(element).click().perform()
		element.send_keys(Keys.ARROW_DOWN).perform()
		element.send_keys(Keys.ARROW_DOWN).perform()
		element.send_keys(Keys.RETURN).perform()
