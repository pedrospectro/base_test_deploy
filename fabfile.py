import os
import glob
import json
import time
import getpass
import unittest
from fabric.api import *
from fabric.contrib.files import exists
from fabric.contrib.console import confirm

##################################Test DEPLOY :D######################################
#######################    Dev : pedroccavalcante@gmail.com    ############################
#######################        Server Host environments        ############################
##########  You must ensure you have access to this server with a ssh key  ################
###########################################################################################
###########################################################################################
#                                                                                         #
#                            		PROPERTIES                                            #
#                            															  #	
###########################################################################################


stage_host=''
prod_host =''

#SLACK-BOT config
plaform_bot = {
    'api': 'TestBot-Api',
    'frontend': 'TestBot-Front',
}

#Repository folder in server
project_directory = ''
api_directory=''
frontend_directory=project_directory+'/'
new_frontend_directory=project_directory+'project_angular/'

#Repository folder in local
local_project_directory = '~/base_frontend/'
local_api_directory=local_project_directory+'base_rails_api/'
local_frontend_directory=local_project_directory+'/'
local_new_frontend_directory=local_project_directory+'/'

api_repository = ''
frontend_repository = ''
new_frontend_repository = ''

#Url from SLACK to inform about test and deploy
slack_channel_url='https://hooks.slack.com/services/

#####################################################################################################
#                                                                                                   #
#                            		        METHODS                                          	    #
#                            															            #
#####################################################################################################

def stage():
    env.user = 'user_stage'
    env.password = os.environ['project_STAGE_PASSWORD']
    env.hosts = [stage_host]

def prod():
    env.user = 'user_prod'
    env.password = os.environ['project_PROD_PASSWORD']
    env.hosts = [prod_host]


#Send message with user and emoji to slack
def send_to_slack(user,message, emoji):
    data = json.dumps({
        "channel": "#slack_channel",
        "username": user,
        "text": message,
        "icon_emoji": ':' + emoji + ':'
    })
    
    data = data.replace('"', '\\"')
    url = slack_channel_url
    local('curl -H "Content-Type: application/json" -X POST -d "{}" {}'.format(data, url))

def send_test_results_to_slack(success, platform, user, details , api_test_result):
    if success:
        msg = '*Test successful!*\n*Developer:* _{}_'.format(user)
        icon = 'video_game'
    else:
        msg = '*Test failed!*\n*Developer:* _{}_\n*Details:*\n{} {}'.format(user, details,api_test_result)
        icon = 'oncoming_police_car'
    send_to_slack(plaform_bot[platform], msg, icon)

#Api Test and stop test

def test_api(user):
    local("cd "+local_api_directory+" && bundle install")
    local("cd "+local_api_directory+" && rails db:drop")
    local("cd "+local_api_directory+" && rails db:create")
    local("cd "+local_api_directory+" && rails db:migrate")
    local("cd "+local_api_directory+" && rails db:seed")
    local("cd "+local_api_directory+" && rails s -b 0.0.0.0 -p 4000 &")
    #wait rails api server works
    time.sleep( 5 )

    failed = 0
    send_to_slack(user,"Iniciando Teste da API", "hammer_and_wrench")
    # Execute API tests
    test_list_dir = ['api/']
    for directory in test_list_dir:
        for file_path in os.listdir(directory):
            command = 'jasmine-node '+directory+file_path+' --junitreport'
            with settings(warn_only=True):
                result = local(command, capture=True)
            if result.failed:
                send_test_results_to_slack( not result.failed, 'api', user, command , None)
                failed=1
                local('kill -INT $(cat '+local_api_directory+'tmp/pids/server.pid)')
                return False

    if failed==0:
        send_test_results_to_slack( True, 'api', user, None , None)
        local('kill -INT $(cat '+local_api_directory+'tmp/pids/server.pid)')
        return True

def stop_test_api():
    #stop local rails backend
    pass


#Front Test and stop test
def start_test_local_frontend(user):
    #local("cd "+local_api_directory+" && rails s -b 0.0.0.0 -p 4000 &")
    local("cd "+local_new_frontend_directory+" && gulp &")
    #wait gulp
    time.sleep(8)
    
    send_to_slack(user,"Iniciando Teste Local do FrontEND", "hammer_and_wrench")
    
    #TODO 
    #Exec front tests
    testsuite = unittest.TestLoader().discover('new_front/.')
    ret = unittest.TextTestRunner(verbosity=1).run(testsuite)
    errors_and_failures = ret.errors + ret.failures
    cleaned_errors = []
    #get errors
    if len(errors_and_failures) > 0:
        for error in errors_and_failures:
            # Only the first part of the string is needed
            cleaned_errors.append(str(error[0]).split(' ')[0])
    # Check error or failure existence
    success = len(ret.errors) == 0 and len(ret.failures) == 0
    details = '' if success else '\n'.join(cleaned_errors)
    ##Send message and return test results
    send_test_results_to_slack(success, 'frontend', user, details , None)
    #send_test_results_to_slack(True, 'frontend', user, None , None)
    if success:
        return True
    else:
        return False

def stop_test_local_front():
    #stop gulp watch and rails on local machine
    #local('kill -INT $(cat '+local_api_directory+'tmp/pids/server.pid)')
    local('pkill gulp')

def test_local_frontend(user):
    start_test_local_frontend(user)
    stop_test_local_front()

#Get selected host args
def host_selected():
	env_selected=''
	if(env.hosts[0]==stage_host):
		env_selected = 'stage'
	if(env.hosts[0]==prod_host):
		env_selected = 'prod'	
	return env_selected

#Sending local updates to git (Runs is and only if the test get success)
def git_update_changes(user,commit_msg,repository_dir):
    local("cd "+repository_dir+" && git pull")
    local("cd "+repository_dir+" && git add .")        
    with settings(warn_only=True):
		local("cd "+repository_dir+" && git commit -am '"+commit_msg+"'")
    send_to_slack(user,'Commit: '+commit_msg,'octocat')
    local("cd "+repository_dir+" && git push")
    #wait time to push new files
    time.sleep(5)

def git_update_changes_front(user,commit_msg,repository_dir):
    local("cd "+repository_dir+" && git pull origin layout-novo")
    local("cd "+repository_dir+" && git add .")        
    with settings(warn_only=True):
		local("cd "+repository_dir+" && git commit -am '"+commit_msg+"'")
    send_to_slack(user,'Commit: '+commit_msg,'octocat')
    local("cd "+repository_dir+" && git push")
    #wait time to push new files
    time.sleep(5)

#Test and Deploy API with user and commit_msg.
def deploy_api(user,commit_msg):
	current_env=host_selected()
	if((env.hosts[0]==stage_host)or(env.hosts[0]==prod_host)):
		api_ok=test_api(user)
		stop_test_api()
		if api_ok:
			#Setup and commit local changes
			git_update_changes(user,commit_msg,local_api_directory)
			#Load Changes in server
			send_to_slack('start-deploy-bot',':pill: Iniciando Deploy da API em '+current_env,'construction')
			run("cd "+api_directory+" && sudo git pull")
			#Install new backend dependencies and run migrate if and only if there are new pending migrates
			run("cd "+api_directory+" && bundle install")
			run("cd "+api_directory+" && rails db:migrate")
			run("cd "+api_directory+" && whenever --update-crontab")
			#Let's rock the API
			send_to_slack(user,'Finalizando Deploy da API em '+current_env,'checkered_flag')

#Test and Deploy Front with user and commit_msg.
def deploy_front(user,commit_msg):
    current_env=host_selected()
    if((env.hosts[0]==stage_host)or(env.hosts[0]==prod_host)):
        front_ok=start_test_local_frontend(user)
        stop_test_local_front()
        if front_ok:
            #Setup and commit local changes
            git_update_changes_front(user,commit_msg,local_new_frontend_directory)
            send_to_slack('start-deploy-bot',':pill: Iniciando Deploy do Novo Front em '+current_env,'construction')
            if env.hosts[0]==stage_host:
                local("cd "+local_new_frontend_directory+" && npm i && gulp stage")
            else:
                local("cd "+local_new_frontend_directory+" && npm i && gulp prod")

            local("cd "+local_new_frontend_directory+" && scp -rp build/* "+env.user+"@"+env.hosts[0]+":~")    

            send_to_slack(user,'Finalizando Deploy do Novo Front em '+current_env,'checkered_flag')